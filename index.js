// console.log("duong");

// Bài 1: Quản lý tuyển sinh

// hàm so sánh điểm
soSanhDiem = function (x, y) {
  if (x >= y) {
    return "Bạn đã đậu.";
  } else return "Bạn đã rớt.";
};

// hàm kiểm tra số điểm người dùng nhập vào có bị sai hay không

kiemTraVungDiem = function (a, b, c, x) {
  if (x > 34.5 || x < 0) {
    return "Nhập lại điểm chuẩn";
  } else if (a > 10 || b > 10 || c > 10 || a < 0 || b < 0 || c < 0) {
    return "Nhập lại điểm các môn";
  } else return 1;
};

document.getElementById("soSanhDiemChuan").onclick = function () {
  // alert(123);
  var diemChuan = document.getElementById("diemChuan").value * 1;
  var diemKhuvuc = document.getElementById("khuVuc").value * 1;
  var diemDoiTuong = document.getElementById("doiTuong").value * 1;

  var diemMon1 = document.getElementById("diemMon1").value * 1;
  var diemMon2 = document.getElementById("diemMon2").value * 1;
  var diemMon3 = document.getElementById("diemMon3").value * 1;

  var diemTong = diemMon1 + diemMon2 + diemMon3 + diemKhuvuc + diemDoiTuong;

  var ketQuaIn = "";

  if (kiemTraVungDiem(diemMon1, diemMon2, diemMon3, diemChuan) != 1) {
    ketQuaIn = kiemTraVungDiem(diemMon1, diemMon2, diemMon3, diemChuan);
  } else if (diemMon1 <= 0 || diemMon2 <= 0 || diemMon3 <= 0) {
    ketQuaIn = "Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0";
  } else {
    ketQuaIn = soSanhDiem(diemTong, diemChuan) + " Tổng điểm: " + diemTong;
  }

  document.getElementById("ketQuaSoSanhDiemChuan").innerHTML = ketQuaIn;
};

// Bài 2: Tính tiền điện

tinhGiaDien = function (x) {
  if (x <= 50) {
    return x * 500;
  } else if (x <= 50 + 50) {
    return 50 * 500 + (x - 50) * 650;
  } else if (x <= 50 + 50 + 100) {
    return 850;
  } else if (x <= 50 + 50 + 100 + 150) {
    return 1100;
  } else if (x <= 50 + 50 + 100 + 150) {
    return 1300;
  }
};
// console.log("tinhGiaDien: ", tinhGiaDien(51));

document.getElementById("tinhTienDien").onclick = function () {
  // alert(1243);

  var hoTen = document.getElementById("hoTen").value;

  var soKw = document.getElementById("soKw").value * 1;
  if (soKw == 0) {
    alert("Nhập số tiền điện");
  }
  var tienDien = 0;

  if (soKw <= 50) {
    tienDien = 500 * soKw;
  } else if (soKw <= 50 + 50) {
    tienDien = 500 * 50 + 650 * (soKw - 50);
  } else if (soKw <= 50 + 50 + 100) {
    tienDien = 500 * 50 + 650 * 50 + 850 * (soKw - 50 - 50);
  } else if (soKw <= 50 + 50 + 100 + 150) {
    tienDien = 500 * 50 + 650 * 50 + 850 * 100 + 1100 * (soKw - 50 - 50 - 100);
  } else if (soKw > 50 + 50 + 100 + 150) {
    tienDien =
      500 * 50 +
      650 * 50 +
      850 * 100 +
      1100 * 150 +
      1300 * (soKw - 50 - 50 - 100 - 150);
  }

  document.getElementById("tienDien").innerHTML =
    "Họ tên: " + hoTen + "; Tiền điện: " + tienDien.toLocaleString() + " vnđ";
};

// Bài 3: Tính thuế thu nhập cá nhân

//hàm tính thuế suất (triệu đống)
tinhThueSuat = function (x) {
  if (x <= 60) {
    return 0.05;
  } else if (x <= 120) {
    return 0.1;
  } else if (x <= 210) {
    return 0.15;
  } else if (x <= 384) {
    return 0.2;
  } else if (x <= 624) {
    return 0.25;
  } else if (x <= 960) {
    return 0.3;
  } else if (x > 960) {
    return 0.35;
  }
};
// console.log("tinhThueSuat: ", tinhThueSuat(75));

document.getElementById("tinhTienThue").onclick = function () {
  // alert(234);
  var hoTen = document.getElementById("hoTentienThue").value;
  var tongThuNhapNam = document.getElementById("tongThuNhapNam").value * 1;
  var soNguoiPhuThuoc = document.getElementById("soNguoiPhuThuoc").value * 1;

  var thuNhapChiuThue = tongThuNhapNam - 4000000 - soNguoiPhuThuoc * 1600000;
  console.log("thuNhapChiuThue: ", thuNhapChiuThue);

  if (thuNhapChiuThue <= 0) {
    document.getElementById("tienThue").innerHTML =
      hoTen + " chưa đủ giàu để nộp thuế";
  } else {
    var tienThue = thuNhapChiuThue * tinhThueSuat(thuNhapChiuThue / 1000000);

    // console.log("tinhThueSuat: ", tinhThueSuat(thuNhapChiuThue / 1000000));

    document.getElementById("tienThue").innerHTML =
      "Họ tên: " +
      hoTen +
      "; Tiền thuế thu nhập cá nhân: " +
      tienThue.toLocaleString() +
      " vnđ";
  }
};

// Bài 4: Tính tiền cáp

// hàm xoá class d-none
function removeClass() {
  let element = document.getElementById("soKetNoi");
  element.classList.remove("d-none");
}

// hàm thêm class d-none
function addClass() {
  let ele = document.getElementById("soKetNoi");
  ele.classList.add("d-none");
}

// hàm ẩn hiện số kênh kết nối
function myFunction() {
  var x = document.getElementById("loaiKhachHang").value;

  if (x == 2) {
    removeClass();
  } else if (x != 2) {
    addClass();
  }
}

// tính Tiền cáp
document.getElementById("tinhTienCap").onclick = function () {
  // alert(123);

  var loaiKhachHang = document.getElementById("loaiKhachHang").value * 1;
  var maKhachHang = document.getElementById("maKhachHang").value;
  var soKenhCaoCap = document.getElementById("soKenhCaoCap").value * 1;

  var soKetNoi = document.getElementById("soKetNoi").value * 1;
  // console.log("soKetNoi: ", soKetNoi);

  var phiHoaDon = 0;
  var phiCoBan = 0;
  var phiKenhCaoCap = 0;

  // console.log("loaiKhachHang: ", loaiKhachHang);

  switch (loaiKhachHang) {
    case 0: {
      return alert("Chọn kiểu khách hàng đi bạn ơi");
    }
    case 1: {
      phiHoaDon = 4.5;
      phiCoBan = 20.5;
      phiKenhCaoCap = 7.5;
      break;
    }
    case 2: {
      phiHoaDon = 15;
      phiKenhCaoCap = 50;

      if (soKetNoi <= 10) {
        phiCoBan = 75;
      } else {
        phiCoBan = 75 + 5 * (soKetNoi - 10);
      }
      break;
    }
  }

  var tienCap = phiHoaDon + phiCoBan + phiKenhCaoCap * soKenhCaoCap;

  document.getElementById("tienCap").innerHTML =
    "Mã khách hàng: " +
    maKhachHang +
    "; Tiền cáp: $" +
    tienCap.toLocaleString(undefined, {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2,
    });
};
